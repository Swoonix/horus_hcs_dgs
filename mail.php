<?php

function getIfSet(&$value, $default = " ")
{
    return isset($value) ? $value : $default;
}

 $mail_To = "dgs.coach@gmail.com";
 $mail_Subject = getIfSet($_POST['sujet'], "Nouveau mail depuis le formulaire de contact")."\n";

 $headers  = getIfSet($_POST['name'], "Aucun nom")."\n"; 
 $headers .= "Reply-To: ". getIfSet($_POST['email'], "Aucun email")."\n";
 $headers .= "MIME-Version: 1.0\r\n";
 $headers .= "Content-Transfer-Encoding: 8bit\n";
 $headers .= "Content-type: text/html; charset=utf-8\n";
           
 $mail_Body = getIfSet($_POST['message'], "Aucun message")."\n";

 if(mail($mail_To, $mail_Subject, $mail_Body, $headers)) 
    { 
        header('Location: /');
    } 
    else 
    { 
        header('Location: /');
    } 
?>
